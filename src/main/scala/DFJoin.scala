import org.apache.spark.{SparkConf, SparkContext}
import org.apache.log4j.{Level, Logger}
import com.databricks.spark.avro._
import org.apache.spark.sql.hive.HiveContext

object DFJoin {
  val rootLogger: Logger = Logger.getRootLogger
  rootLogger.setLevel(Level.ERROR)
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  Logger.getLogger("kafka").setLevel(Level.OFF)
  Logger.getLogger("spark").setLevel(Level.OFF)
  Logger.getLogger("yarn.Client").setLevel(Level.OFF)

  def main(args: Array[String]): Unit = {
    val config = new SparkConf()
      .setAppName("dfjoin")
      .setMaster("local[4]")

    val sc = new SparkContext(config)
    val sqlContext = new HiveContext(sc)
    val customersDF = sqlContext.read.avro("data/retail_db_avro/customers")
    val ordersDF = sqlContext.read.avro("data/retail_db_avro/orders")
    val order_itemsDF = sqlContext.read.avro("data/retail_db_avro/order_items")
    val categoriesDF = sqlContext.read.avro("data/retail_db_avro/categories")
    val productsDF = sqlContext.read.avro("data/retail_db_avro/products")

    //find customers without any orders
    import sqlContext.implicits._
    customersDF.join(ordersDF, customersDF("customer_id") === ordersDF("order_customer_id"), "leftouter")
      .filter($"order_customer_id".isNull)
      .select($"customer_id", $"customer_fname", $"customer_lname")
      .show

    //join orders, order_items,products, categories
    val joinedDF = ordersDF.join(order_itemsDF, ordersDF("order_id") === order_itemsDF("order_item_order_id"))
      .join(productsDF, order_itemsDF("order_item_product_id") === productsDF("product_id"))
      .join(categoriesDF, productsDF("product_category_id") === categoriesDF("category_id")).cache

    //sort categories by total income desc
    import org.apache.spark.sql.functions._
    joinedDF.groupBy($"category_id", $"category_name")
      .agg(round(sum($"order_item_subtotal"),2).alias("sum"))
      .orderBy($"sum".desc)
      .show

    // find 3 most popular products in each category
    import org.apache.spark.sql.expressions.Window
    val partWindow = Window.partitionBy($"category_id").orderBy($"cnt".desc)
    joinedDF.groupBy($"category_id", $"product_id", $"product_name")
      .agg(count($"*").alias("cnt"))
      .select($"category_id", $"product_id", $"product_name", $"cnt", rank.over(partWindow).alias("rank"))
      .filter($"rank" <= 3)
      .orderBy($"category_id", $"rank")
      .show
  }
}
