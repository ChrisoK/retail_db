import org.apache.spark.{RangePartitioner, SparkConf, SparkContext}
import org.apache.log4j.{Level, Logger}

object RddReduceSample {
  val rootLogger: Logger = Logger.getRootLogger
  rootLogger.setLevel(Level.ERROR)
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  Logger.getLogger("kafka").setLevel(Level.OFF)
  Logger.getLogger("spark").setLevel(Level.OFF)
  Logger.getLogger("yarn.Client").setLevel(Level.OFF)

  def main(args: Array[String]): Unit = {
    val config = new SparkConf()
      .setAppName("rdd2df")
      .setMaster("local[4]")
    val sc = new SparkContext(config)

    def time[R](block: => R): R = {
      val t0 = System.nanoTime()
      val result = block
      val t1 = System.nanoTime()
      println("Elapsed time: " + (t1 - t0) + "ns")
      result
    }

    //count the number of transactions per month

    val ordersYM_Rdd = sc.textFile("data/retail_db_txt/orders")
      .map(_.split("\t"))
      .map(r => r(1).substring(0, 7))

    //groupBy

    time(
      ordersYM_Rdd.map((_, 1))
        .groupByKey
        .map(r => (r._1, r._2.reduce(_ + _)))
        .coalesce(1)
        .sortBy(_._2, ascending = false)
        .foreach(println)
    )

    //reduceByKey
    time(
      ordersYM_Rdd.map((_, 1)).reduceByKey(_ + _, 1)
        .sortBy(_._2, ascending = false)
        .foreach(println)
    )

    //combineByKey
    val createCombiner = (v: Int) => v
    val mergeValues = (acc: Int, n: Int) => acc + n
    val mergeCombiners = (acc1: Int, acc2: Int) => acc1 + acc2
    time(
      ordersYM_Rdd.map((_, 1))
        .combineByKey(
          createCombiner,
          mergeValues,
          mergeCombiners,1)
        .sortBy(_._2, ascending = false)
        .foreach(println)
    )


  }

}
