import org.apache.spark.{ SparkContext, SparkConf }
import org.apache.spark.sql.SQLContext

object rdd2df {
  def main(args: Array[String]): Unit = {
    val config = new SparkConf()
      .setAppName("rdd2df")
      .setMaster("local[4]")
    val sc =new SparkContext(config)
    val sqlContext = new SQLContext(sc)
    val customersRdd = sc.textFile("data/retail_db_txt/customers")
    import sqlContext.implicits._
    val customerDF = customersRdd
      .map(_.split("\t"))
      .map(r=>(r(0).toInt,r(1),r(2),r(3),r(4),r(5),r(6),r(7),r(8)))
      .toDF("id","name","last_name","e-mail","password","street","city","state","zipcode")
    customerDF.show
  }
}
